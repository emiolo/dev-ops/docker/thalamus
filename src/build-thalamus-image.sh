#!/bin/sh

imageName=$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
releaseTag=$CI_REGISTRY_IMAGE:latest

if ! docker info &>/dev/null; then
  if ! [ -z ${KUBERNETES_PORT+x} ]; then
    export DOCKER_HOST='tcp://localhost:2375';
  fi;
fi

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

if [ ! -f "./Dockerfile" ]; then
  useDefaultDockerfile=true
else
  useDefaultDockerfile=false
fi

if [ $useDefaultDockerfile ]; then
  echo "### Add default Dockerfile..."
  cp -a /builder/Dockerfile .
  echo "### Add default Dockerfile... Done!"
fi

echo "### Build docker image..."
docker build -t project . || exit 1
echo "### Build docker image... Done!"

if [ $useDefaultDockerfile ]; then
  echo "### Remove default Dockerfile..."
  rm Dockerfile
  echo "### Remove default Dockerfile... Done!"
fi

echo "### Create Tags..."
docker tag project $imageName || exit 1
if [ "$CI_COMMIT_REF_SLUG" = "master" ]; then
  docker tag project $releaseTag || exit 1
fi
echo "### Create Tags... Done!"

echo "### Push images..."
docker push $imageName || exit 1
if [ "$CI_COMMIT_REF_SLUG" = "master" ]; then
  docker push $releaseTag || exit 1
fi
echo "### Push images..."
